import React, { Fragment } from "react";

const About = () => {
  return (
    <Fragment>
      <h1>About This App</h1>
      <p className="my-1">App to keep track of your contacts</p>
      <p className="bg-dark p">Version: 1.0.0</p>
    </Fragment>
  );
};

export default About;
